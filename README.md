Backpack.tf-Enhancer
=================

Auto-relister for classifieds

This script automatically bumps your listings on [backpack.tf](http://backpack.tf)'s classifieds

More information is available in the wiki.

Track its development here: [Trello](https://trello.com/b/O7g04M7a/backpack-tf-enhancer)